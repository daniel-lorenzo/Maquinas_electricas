# Maquinas_electricas
Máquinas e instalaciones eléctricas
__UNLZ - FI__   
__Alumno:__ Daniel Antonio Lorenzo 

__Descripción:__ prácticas realizadas por mi durante el curso de máquinas e instalaciones eléctricas en la UNLZ - FI.   
Se ofrece libremente, sin garantía.

## Unidad 0
* [Leyes fundamentales del electromagnetismo](https://nbviewer.jupyter.org/github/daniel-lorenzo/Maquinas_electricas/blob/master/Unidad0/Leyes_fundamentales.ipynb)

## Unidad 1
* [Ejemplo 1 - máquinas corriente continua](https://nbviewer.jupyter.org/github/daniel-lorenzo/Maquinas_electricas/blob/master/Unidad1/Ejemplo_1_maquinas_cc.ipynb)
